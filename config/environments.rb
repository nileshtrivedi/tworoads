configure :production, :development do
 db = URI.parse(ENV['DATABASE_URL'] || 'postgres:///localhost/portfolio')

 ActiveRecord::Base.establish_connection(
   :adapter  => db.scheme == 'postgres' ? 'postgresql' : db.scheme,
   :host     => db.host,
   :username => db.user,
   :password => db.password,
   :database => db.path[1..-1],
   :encoding => 'utf8'
 )
end

=begin
ActiveRecord::Base.establish_connection(
  :adapter =>    'postgresql',
  :host =>       'localhost',
  :database =>   'portfolio',
  :username =>   'postgres',
  :password =>   'postgres',
  :port =>       '5432'
)
=end

