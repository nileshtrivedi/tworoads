require 'sinatra'
require 'active_record'
require 'activerecord-import'
require 'date'
require 'csv'
require './config/environments'

use ActiveRecord::ConnectionAdapters::ConnectionManagement


PRODUCTS = {
    'FMCG' => ['BRITANNIA','DABUR','GILLETTE'],
    'Banking' => ['IDBI','SBI','ICICI'],
    'Power' => ['RELIANCEINFRA','TATAPOWER','NTPC'],
    'Automobile' => ['TATA','MARUTI','HERO'],
    'IT' => ['TCS','INFOSYS','HCL']
} # This should be generated from the DB upon each request to make the app completely data-independent
# Caching it here for better performance

class Tick < ActiveRecord::Base
end

get '/' do
  @sectors = PRODUCTS.keys
  @stocks = PRODUCTS.values.flatten
  erb :newindex, :layout => false
end

get '/newindex' do
  @sectors = PRODUCTS.keys
  @stocks = PRODUCTS.values.flatten
  erb :newindex, :layout => false
end

get '/upload' do
  erb :upload
end

post '/upload' do
  unless params[:upload] &&
         (tmpfile = params[:upload][:tempfile]) &&
         (name = params[:upload][:filename]) && (name.end_with?(".csv") || name.end_with?(".CSV"))
    @error = "No CSV file selected"
    return erb(:upload)
  end
  STDERR.puts "Uploading file, original name #{name.inspect}"
  data = CSV.read(tmpfile)
  STDERR.puts "Upload complete. #{data.size} rows read"
  Tick.delete_all
  ticks = []
  data.each do |row|
    ticks << Tick.new(:sector => row[0], :stock => row[1], :day => row[2], :jd => row[3].to_i, :price => row[4].to_f, :roi => row[5].to_f, :sum_logroi => row[6].to_f, :holiday => (row[7] == 'Y'))
  end
  Tick.import ticks
  @notice = "Upload successful. Database was reset and #{ticks.size} ticks were imported."
  STDERR.puts @notice
  @sectors = PRODUCTS.keys #@ticks.collect(&:sector).uniq
  @stocks = PRODUCTS.values.flatten #@ticks.collect(&:stock).uniq
  erb :newindex, :layout => false
end

post '/query' do
  STDERR.puts "Making a new query"
  qtype = params["qtype"]
  if qtype == 'sector'
    sectors = PRODUCTS.keys.select { |s| params[s + '_weight'].to_f > 0.0001 }
    STDERR.puts "sectors = #{sectors.inspect}"
    weights = {}
    sectors.each { |sector|
      num_stocks = PRODUCTS[sector].length
      PRODUCTS[sector].each { |stock|
        # within each sector, equal weight to each stock
        weights[stock] = params[sector + '_weight'].to_f / num_stocks / 100.0
      }
    }
  elsif qtype == 'stock'
    stocks = PRODUCTS.values.flatten.select { |s| params[s + '_weight'].to_f > 0.0001 }
    STDERR.puts "stocks = #{stocks.inspect}"
    weights = {}
    stocks.each { |s| weights[s] = params[s + '_weight'].to_f / 100.0 }
  end
  STDERR.puts "params = #{params.inspect}"
  STDERR.puts "weights = #{weights.inspect}"
  results = analyze_portfolio(weights, params["start_date"], params["end_date"], params["rebalance_freq"].to_i)
  results.to_json
end

def analyze_portfolio(weights, start_date, end_date, rebalance_freq)
  start_date_jd = Date.parse(start_date).jd
  end_date_jd = Date.parse(end_date).jd
  STDERR.puts "start_date_jd = #{start_date_jd} end_date_jd = #{end_date_jd}"
  ticks = Tick.where("jd BETWEEN ? and ?", start_date_jd, end_date_jd).where("stock in (?)", weights.keys).order("jd ASC")
  #STDERR.puts ticks.inspect
  ticks_daywise = ticks.group_by { |t| t.jd }
  #STDERR.puts ticks_daywise.inspect
  result = []
  prev_value = 100
  prev_weights = weights
  max_drawdown = [0.0,nil]
  holidays = 0
  (start_date_jd..end_date_jd).each_with_index { |jd,i|
    amount = 0
    ticks_daywise[jd].each { |t|
      amount += prev_weights[t.stock].to_f * prev_value * t.roi
    }
    
    # Holidays are those days when ALL the stocks don't have a tick
    # This may not be a universally-accepted assumption but it seems reasonable
    if ticks_daywise[jd].reject { |t| t.holiday }.size == 0
      holidays += 1
    end
    
    if ((i+1) % rebalance_freq == 0)
      # STDERR.puts "Rebalancing on #{i+1}th day"
      # rebalance the weights
      prev_weights = weights
    else
      # re-calculate weights
      new_weights = {}
      ticks_daywise[jd].each { |t|
        new_weights[t.stock] = prev_weights[t.stock].to_f * prev_value * t.roi / amount
      }
      prev_weights = new_weights
    end
    drawdown = (prev_value - amount)*100/prev_value
    if drawdown > max_drawdown[0]
      max_drawdown = [drawdown, Date.jd(jd).to_s]
    end
    result << amount
    prev_value = amount
  }
  net_returns = result.last - 100
  STDERR.puts "Number of holidays in the given range = #{holidays}"
  annual_growth = (result.last/100.0) ** (252.0/(end_date_jd - start_date_jd - holidays))
  annualized_return = (annual_growth - 1.0)*100.0

  garray = [['Date','Value']];
  (start_date_jd..end_date_jd).each_with_index { |jd,i|
    garray.push [Date.jd(jd).to_s,result[i]]
  }
  return {:results => garray, :max_drawdown => max_drawdown,
    :annualized_returns => annualized_return, :net_returns => net_returns, :weights => weights.to_a}
end
