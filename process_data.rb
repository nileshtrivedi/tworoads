require 'date'
require 'csv'
require 'pg'
require 'active_record'

path = "./Data"
File.open("processed_data.csv","w") { |output|
Dir.entries(path).select { |f| File.directory?(File.join(path,f)) }.each { |sector|
  next if sector == '.' or sector == '..'
  puts "Browsing directory #{sector}"
  Dir.entries(File.join(path,sector)).select { |f| File.file?(File.join(path,sector,f)) }.each { |stock|
    next unless stock.end_with?(".csv")
    puts "Processing File: #{stock}"
    data = CSV.read(File.join(path,sector,stock))
    puts "Found #{data.size-1} rows in CSV"
    data.slice!(0)
    prev_jd = nil
    sorted_data = data.sort_by { |r| r[0] } # sort by date ascending
    prev_price = sorted_data.first[1].to_f
    sum_logroi = 0.00
    sorted_data.each { |row|
      day = row[0]
      jd = Date.parse(day).jd
      stock_symbol = stock[0..-5]
      price = row[1].to_f
      if prev_jd && jd > (prev_jd+1) #some days are missing.
        (jd - prev_jd - 1).times { |i|
          tjd = prev_jd + i + 1
          output.puts "#{sector},#{stock_symbol},#{Date.jd(tjd)},#{tjd},#{prev_price},1.00,#{sum_logroi},Y"
        }
      end
      roi = price / prev_price
      sum_logroi += Math.log(roi)
      output.puts "#{sector},#{stock_symbol},#{day},#{jd},#{price},#{roi},#{sum_logroi},N"
      #sector,stock,date,julian_date,price,daily return, cumulative sum of log return, holiday
      prev_jd = jd
      prev_price = price
    }
  }
}
}

=begin
ActiveRecord::Base.establish_connection(
  adapter:    'postgresql',
  host:       'localhost',
  database:   'portfolio',
  username:   'postgres',
  password:   'postgres',
  port:       '5432'
)


CREATE TABLE ticks
(
  id bigserial NOT NULL,
  sector varchar(20) NOT NULL,
  stock varchar(20) NOT NULL,
  day date NOT NULL,
  jd integer NOT NULL,
  price double precision NOT NULL,
  roi float,
  sum_logroi float,
  holiday boolean default false not null,
  CONSTRAINT ticks_pkey PRIMARY KEY (id),
  UNIQUE (stock,day)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ticks
  OWNER TO postgres;
  
CREATE INDEX stock_index on ticks (stock);
CREATE INDEX jd_index on ticks (jd);


class Tick < ActiveRecord::Base
end

Tick.delete_all
all_ticks = CSV.read('processed_data.csv')
all_ticks.each do |row|
  Tick.create(:sector => row[0], :stock => row[1], :day => row[2], :jd => row[3].to_i, :price => row[4].to_f)
end

puts "Inserted #{Tick.count} ticks into the database"

=end
