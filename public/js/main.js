var sectors = ["FMCG", "Banking", "Power", "Automobile", "IT"];
var stocks = ["BRITANNIA", "DABUR", "GILLETTE", "IDBI", "SBI", "ICICI", "RELIANCEINFRA", "TATAPOWER", "NTPC", "TATA", "MARUTI", "HERO", "TCS", "INFOSYS", "HCL"];

function recalculate_weights(){
  var products;
  if ( $("#qtype").val() == "stock"){
    products = stocks;
  } else if ( $("#qtype").val() == "sector"){
    products = sectors;
  }
  var total_amount = 0;  
  for(var i = 0; i < products.length; i++){
    total_amount += parseInt($("#" + products[i] + "_amount").val());
  }
  console.log("total_amount = " + total_amount);
  for(var i = 0; i < products.length; i++){
    var weight = parseInt($("#" + products[i] + "_amount").val()) / total_amount;
    $("#" + products[i] + "_weight").val((weight*100).toFixed(2));
  }
}

//google.setOnLoadCallback

