require 'date'
require 'csv'
require 'pg'
require 'active_record'

ActiveRecord::Base.establish_connection(
  :adapter =>    'postgresql',
  :host =>       'localhost',
  :database =>   'portfolio',
  :username =>   'postgres',
  :password =>   'postgres',
  :port =>       '5432'
)

PRODUCTS = {
    'FMCG' => ['BRITANNIA','DABUR','GILLETTE'],
    'Banking' => ['IDBI','SBI','ICICI'],
    'Power' => ['RELIANCEINFRA','TATAPOWER','NTPC'],
    'Automobile' => ['TATA','MARUTI','HERO'],
    'IT' => ['TCS','INFOSYS','HCL']
}

class Tick < ActiveRecord::Base
end

def analyze_portfolio(weights, start_date, end_date, rebalance_freq)
  start_date_jd = Date.parse(start_date).jd
  end_date_jd = Date.parse(end_date).jd
  
  ticks = Tick.where("jd BETWEEN ? and ?", start_date_jd, end_date_jd).where("stock in (?)", weights.keys.join(',')).order("jd ASC")
  
  ticks_daywise = ticks.group_by { |t| t.jd }
  result = []
  prev_value = 100
  prev_weights = weights
  max_drawdown = [0.0,nil]
  (start_date_jd..end_date_jd).each_with_index { |jd,i|
    amount = 0
    ticks_daywise[jd].each { |t|
      stock_amount = prev_weights[t.stock].to_f * prev_value * t.roi
      amount += stock_amount
    }
    
    if ((i+1) % rebalance_freq == 0)
      STDERR.puts "Rebalancing on #{i+1}th day"
      # rebalance the weights
      prev_weights = weights
    else
      # re-calculate weights
      new_weights = {}
      ticks_daywise[jd].each { |t|
        new_weights[t.stock] = prev_weights[t.stock].to_f * prev_value * t.roi / amount
      }
      STDERR.puts "new_weights = #{new_weights.inspect}"
      prev_weights = new_weights
    end
    drawdown = (prev_value - amount)*100/prev_value
    if drawdown > max_drawdown[0]
      max_drawdown = [drawdown, jd]
    end
    result << amount
    prev_value = amount
  }
  net_returns = result.last - 100
  return {:results => result, :max_drawdown => max_drawdown, :annualized_returns => nil, :net_returns => net_returns}
end


s = analyze_portfolio({'GILLETTE' => 1.00}, '2005-01-03', '2005-01-09',2)


